package com.example.ain427.testproject.data;

/**
 * Created by AIN427 on 12.03.2017.
 */

public class Constants {

    public static final String BASE_URL = "http://services.hanselandpetal.com";
    public static final String PHOTO_URL = BASE_URL + "/photos/";

}
