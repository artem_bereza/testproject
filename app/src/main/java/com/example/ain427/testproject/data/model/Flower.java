package com.example.ain427.testproject.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AIN427 on 11.03.2017.
 */

public class Flower {
    @SerializedName("category")
    @Expose
    public String category;
    @SerializedName("price")
    @Expose
    public Float price;
    @SerializedName("instructions")
    @Expose
    public String instructions;
    @SerializedName("photo")
    @Expose
    public String photo;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("productId")
    @Expose
    public Integer productId;

    private int color;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
