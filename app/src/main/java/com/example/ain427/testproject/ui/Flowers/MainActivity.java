package com.example.ain427.testproject.ui.Flowers;

import android.graphics.Canvas;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.example.ain427.testproject.R;
import com.example.ain427.testproject.data.model.Flower;
import com.example.ain427.testproject.ui.base.BaseActivity;

import java.util.List;


public class MainActivity extends BaseActivity<FlowersPresenter> implements FlowersView {

    FlowersAdapter mAdapter;
    RecyclerView mRecyclerView;


    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycle_flowerlist);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);


        mAdapter = new FlowersAdapter(this);
        mRecyclerView.setAdapter(mAdapter);

        FlowersPresenter flowersPresenter = new FlowersPresenter();
        flowersPresenter.attachView(this);
        flowersPresenter.getFlowerList();
        initSwipe();
    }

    private void initSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                mAdapter.remove(position);
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_main;
    }


    @Override
    public void afterFailedRequest(String message) {

    }

    @Override
    public void afterSuccessGetFlowers(List<Flower> flowers) {
        mAdapter.setData(flowers);
    }
}
