package com.example.ain427.testproject.data.service;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MyService extends Service {

    public static final String ACTION = "TEST_ACTION";

    public static final int notify = 150000; //120 000
    private Handler mHandler = new Handler();
    private Timer mTimer = null;

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        if (mTimer != null) // Cancel if already existed
            mTimer.cancel();
        else
            mTimer = new Timer();   //recreate new
        mTimer.scheduleAtFixedRate(new TimeDisplay(this), 0, notify);   //Schedule task
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTimer.cancel();
    }

    private class TimeDisplay extends TimerTask {

        private Context mContext;

        public TimeDisplay(Context context) {
            mContext=context;
        }

        @Override
        public void run() {
            mHandler.post(() -> {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
                Intent in = new Intent(ACTION);
                in.putExtra("resultCode", Activity.RESULT_OK);
                in.putExtra("resultValue", "Текущее время - "+simpleDateFormat.format(new Date()));
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(in);
            });
        }
    }
}
