package com.example.ain427.testproject.ui.Flowers.details;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ain427.testproject.R;
import com.example.ain427.testproject.ui.base.BaseActivity;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

/**
 * Created by AIN427 on 12.03.2017.
 */

public class FlowersDetailsActivity extends BaseActivity<FlowersDetailsPresenter> implements FlowersDetailsView {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.flowerImage)
    ImageView mImageView;
    @BindView(R.id.image_details_instruction)
    TextView textInstruction;

    @Override
    protected void onViewReady(Bundle savedInstanceState) {

        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        setSupportActionBar(mToolbar);

        if (getIntent() != null) {
            getSupportActionBar().setTitle(getIntent().getStringExtra("name"));

            textInstruction.setText(getIntent().getStringExtra("instruction"));
            Picasso.with(this).load(getIntent().getStringExtra("photo")).into(mImageView);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    supportFinishAfterTransition();
                }else finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            supportFinishAfterTransition();
        }else finish();
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_flower_details;
    }
}
