package com.example.ain427.testproject.ui.base;

/**
 * Created by Mann on 02.12.2016.
 */

public class BasePresenter<V extends BaseMvpVIew> implements Presenter<V> {

    V view;


    public V getView() {
        return view;
    }

    @Override
    public void attachView(V v) {
        view = v;
    }

    @Override
    public void detachView() {
        view=null;
    }
}
