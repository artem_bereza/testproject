package com.example.ain427.testproject.ui.Flowers;

import com.example.ain427.testproject.data.model.Flower;
import com.example.ain427.testproject.ui.base.BaseMvpVIew;
import com.example.ain427.testproject.ui.base.BasePresenter;

import java.util.List;

/**
 * Created by AIN427 on 11.03.2017.
 */

public interface FlowersView extends BaseMvpVIew<FlowersPresenter> {

    void afterSuccessGetFlowers(List<Flower> flowers);

    void afterFailedRequest(String message);

}
