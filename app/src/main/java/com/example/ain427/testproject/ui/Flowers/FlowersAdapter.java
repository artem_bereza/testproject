package com.example.ain427.testproject.ui.Flowers;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ain427.testproject.R;
import com.example.ain427.testproject.data.model.Flower;
import com.example.ain427.testproject.ui.Flowers.details.FlowersDetailsActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.ain427.testproject.data.Constants.PHOTO_URL;

/**
 * Created by AIN427 on 11.03.2017.
 */

public class FlowersAdapter extends RecyclerView.Adapter<FlowersAdapter.FlowersHolder> {

    private List<Flower> data = new ArrayList<>();

    private Context mContext;

    public FlowersAdapter(Context mContext) {
        this.mContext = mContext;
    }


    @Override
    public FlowersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);
        return new FlowersHolder(v);
    }

    @Override
    public void onBindViewHolder(FlowersHolder holder, int position) {
        final Flower item = data.get(position);
        Picasso.with(holder.itemView.getContext()).load(PHOTO_URL + item.photo).into(holder.flowerImage);
        holder.flowerName.setText(item.getName());
        holder.flowerCategory.setText(item.getCategory());
        holder.flowerPrice.setText(String.valueOf(item.getPrice()));
        holder.flowerInstruction.setText(item.getInstructions());


        if (item.getColor() == 0) {
            item.setColor(Color.WHITE);
            holder.cardView.setCardBackgroundColor(Color.WHITE);
        } else holder.cardView.setCardBackgroundColor(item.getColor());


        holder.rootView.setOnClickListener((v) -> {

            ColorStateList cardBackgroundColor = holder.cardView.getCardBackgroundColor();

            if (cardBackgroundColor.getDefaultColor() == Color.WHITE) {
                item.setColor(Color.argb(new Random().nextInt(255), new Random().nextInt(255), new Random().nextInt(255), new Random().nextInt(255)));
                holder.cardView.setCardBackgroundColor(item.getColor());
            } else {
                item.setColor(Color.WHITE);
                holder.cardView.setCardBackgroundColor(Color.WHITE);
            }

            Intent intent = new Intent(mContext, FlowersDetailsActivity.class);

            intent.putExtra("photo", PHOTO_URL + item.photo);
            intent.putExtra("instruction", item.getInstructions());
            intent.putExtra("name", item.getName());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                Pair<View, String> p1 = Pair.create(holder.flowerImage, "flowerImage");
                Pair<View, String> p2 = Pair.create(holder.flowerInstruction, "flowerInstruction");
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(((AppCompatActivity) mContext), p1, p2);
                mContext.startActivity(intent, options.toBundle());
            } else {
                mContext.startActivity(intent);
            }
        });
    }

    public void remove(int position) {
        data.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, data.size());
    }

    public void setData(List<Flower> data) {
        this.data = data;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class FlowersHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.flower_cardview)
        CardView cardView;
        @BindView(R.id.flowerImage)
        ImageView flowerImage;
        @BindView(R.id.flowerName)
        TextView flowerName;
        @BindView(R.id.flowerCategory)
        TextView flowerCategory;
        @BindView(R.id.flowerPrice)
        TextView flowerPrice;
        @BindView(R.id.flowerInstruction)
        TextView flowerInstruction;
        @BindView(R.id.item_root)
        RelativeLayout rootView;


        public FlowersHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
