package com.example.ain427.testproject.ui.base;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ain427.testproject.R;
import com.example.ain427.testproject.data.service.MyService;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;

/**
 * Created by Mann on 02.12.2016.
 */

public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity implements BaseMvpVIew<P> {

    private View dialogView;
    private TextView dialogText;
    private ImageView imageDialogg;
    private AlertDialog alertDialog;
    private Context mContext = this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        ButterKnife.bind(this);

        new Handler().postDelayed(() -> startService(new Intent(this, MyService.class)), 10000);

        dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_layout, null, false);
        dialogText = (TextView) dialogView.findViewById(R.id.text_dialog_time);
        imageDialogg = (ImageView) dialogView.findViewById(R.id.image_dialog);

        onViewReady(savedInstanceState);
    }

    private BroadcastReceiver testReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int resultCode = intent.getIntExtra("resultCode", RESULT_CANCELED);
            if (resultCode == RESULT_OK) {
                if (mContext != null) {
                    dialogText.setText(intent.getStringExtra("resultValue"));
                    Picasso.with(mContext).load("http://zellox.com/wp-content/uploads/2016/05/logo-android-png-%E2%80%AB211740713%E2%80%AC-%E2%80%AB%E2%80%AC-1280x1280.png").into(imageDialogg);

                    if (alertDialog == null) {

                        alertDialog = new AlertDialog.Builder(mContext)
                                .setView(dialogView)
                                .setPositiveButton("OK", (dialog, which) -> dialog.cancel())
                                .create();
                        alertDialog.show();
                    } else {
                        alertDialog.show();
                    }

                    new Handler().postDelayed(alertDialog::cancel, 5000);
                }
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(MyService.ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(testReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(testReceiver);
    }

    protected abstract void onViewReady(Bundle savedInstanceState);
}
