package com.example.ain427.testproject.ui.base;

/**
 * Created by Mann on 02.12.2016.
 */

public interface Presenter<V extends BaseMvpVIew> {

    void attachView(V v);
    void detachView();
}
