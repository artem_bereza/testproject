package com.example.ain427.testproject.data.api;

import com.example.ain427.testproject.data.model.Flower;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import rx.Observable;

import static com.example.ain427.testproject.data.Constants.BASE_URL;

/**
 * Created by AIN427 on 11.03.2017.
 */

public interface FlowerAPIService {



    @GET("/feeds/flowers.json")
    Observable<List<Flower>> getFlowers();

    class Factory {

        private static FlowerAPIService service;

        public static FlowerAPIService getInstance() {

            if (service == null) {
                Retrofit retrofit = new Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .baseUrl(BASE_URL).build();
                service = retrofit.create(FlowerAPIService.class);
                return service;
            } else {
                return service;
            }
        }
    }

}
