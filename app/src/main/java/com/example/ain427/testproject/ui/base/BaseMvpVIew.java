package com.example.ain427.testproject.ui.base;

import android.support.annotation.LayoutRes;

/**
 * Created by Mann on 02.12.2016.
 */

public interface BaseMvpVIew<T extends BasePresenter> {

    @LayoutRes
    int getLayoutResource();
}
