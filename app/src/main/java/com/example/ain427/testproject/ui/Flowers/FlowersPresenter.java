package com.example.ain427.testproject.ui.Flowers;

import com.example.ain427.testproject.data.api.FlowerAPIService;
import com.example.ain427.testproject.data.model.Flower;
import com.example.ain427.testproject.ui.base.BasePresenter;


import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by AIN427 on 11.03.2017.
 */

public class FlowersPresenter extends BasePresenter<FlowersView> implements Observer<List<Flower>> {

    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private FlowerAPIService flowerAPIService;


    public void getFlowerList() {

        Observable<List<Flower>> observable = FlowerAPIService.Factory.getInstance().getFlowers();
        compositeSubscription.add(observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .toSingle()
                .subscribe(this));


    }

    @Override
    public void onCompleted() {
        //do nothing
    }

    @Override
    public void onError(Throwable e) {
        //do nothing
    }

    @Override
    public void onNext(List<Flower> flowers) {
        getView().afterSuccessGetFlowers(flowers);
    }


    @Override
    public void attachView(FlowersView flowersView) {
        compositeSubscription.clear();
        super.attachView(flowersView);
    }
}
